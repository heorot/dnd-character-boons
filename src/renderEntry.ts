

const REGEX_ENTRY = /\{@([A-Za-z]+) ([^{]+?)\}/g;
const REGEX_SPELL_LEVEL = /level=(\d);(\d)/g;

function renderAnchor(match: string, a: string, b: string) {
    let page = "";
    let query = "";
    let href = "";
    let innerHTML = b.split("|")[0];
    switch (a) {
        case "spell":
            page = "spells";
            let [spell, spell_book] = b.split("|");
            query = `${spell}_${spell_book || "phb"}`;
            break;
        case "filter":
            let [_, _page, ..._query] = b.split("|");
            page = _page;
            query = _query.join(",");
            if (_page == "items") {
                query = query.replace("type=", "") + "_phb";
            }
            if (_page == "spells") {
                query = _query.map(queryPart => {
                    return queryPart.replace(REGEX_SPELL_LEVEL, "flstlevel:0=$1~1=$2");
                }).join(",");
            }
            break;
        case "feat":
            page = "feats";
            query = b.replace("|", "_");
            break;
        case "creature":
            page = "bestiary";
            let [creature, creature_book] = b.split("|");
            query = `${creature}_${creature_book || "mm"}`;
            break;
        case "condition":
        case "status":
            page = "conditionsdiseases";
            let [condition, _conditionbook] = b.split("|");
            query = `${condition}_${_conditionbook || "phb"}`;
            break;
        case "quickref":
            page = "quickreference";
            let [ref_topic, ref_book, ref_a, ref_b, ref_subject] = b.split("|");
            query = `bookref-quick,${ref_a},${ref_topic},${ref_b}`;
            innerHTML = ref_subject || ref_topic;
            break;
        case "item":
            page = "items";
            query = b.replace("|", "_");
            break;
        case "variantrule":
            page = "variantrules";
            query = b.replace("|", "_");
            break;
        case "action":
            page = "actions";
            query = b + "_phb";
            break;

    }
    if (page)
        href = `https://5e.tools/${page}.html#${encodeURI(query)}`;

    return `<a ${href && ("href=" + href)} target="__blank" rel="noopener">${innerHTML}</a>`;
}

export default function renderEntry(entry: any) {
    const type = entry?.type || typeof entry;
    switch (type) {
        case "string":
            return `<p>
                ${entry.replaceAll(REGEX_ENTRY, renderAnchor)}
                </p>`;

        case "list":
            return `<ul>
                    ${entry.items.map(renderEntry).join("")}
                    </ul>
                    `;
        case "item":
            return `<li>
                    <span class="font-bold">${entry.name} </span>
                    ${entry.entries.map(renderEntry).join("")}
                    </li>`;
        case "table":
            return `
                    ${entry.caption ? `<span class="caption">${entry.caption}</span>` : ""}
                    <table>
                    <tr>
                    ${entry.colLabels.map((label: string) => `<th>${label}</th>`).join("")}
                    </tr>
                    ${entry.rows.map((rowItem: []) => `<tr>
                    ${rowItem.map(cellItem => `<td>${renderEntry(cellItem)}</td>`).join("")}
                    </tr>`).join("")}
                    </table>`;
        case "section":
            return entry.entries.map(renderEntry).join("");
        case "entries":
            return `<div>
                    ${entry.name ? `<h4>${entry.name}</h4>` : ''}
                    ${entry.entries.map(renderEntry).join("")}
                    </div>`;
    }

    return entry;
}