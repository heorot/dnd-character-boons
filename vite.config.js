import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  base: process.env.VITE_BASE_PATH,
  build: {
    rollupOptions: {
      output: {
        manualChunks: (id) => {
          console.log("manualChunks", id);
          if (id.includes("5etools-mirror-2.github.io")) {
            return id.split("/").at(-2);
          }
        }
      }
    }
  }
})
