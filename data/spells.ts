import AAG from "../5etools-mirror-2.github.io/data/spells/spells-aag.json";
import AI from "../5etools-mirror-2.github.io/data/spells/spells-ai.json"
import AitFRAVT from "../5etools-mirror-2.github.io/data/spells/spells-aitfr-avt.json"
import BMT from "../5etools-mirror-2.github.io/data/spells/spells-bmt.json"
import DoDk from "../5etools-mirror-2.github.io/data/spells/spells-dodk.json"
import EGW from "../5etools-mirror-2.github.io/data/spells/spells-egw.json"
import FTD from "../5etools-mirror-2.github.io/data/spells/spells-ftd.json"
import GGR from "../5etools-mirror-2.github.io/data/spells/spells-ggr.json"
import GHLoE from "../5etools-mirror-2.github.io/data/spells/spells-ghloe.json"
import HWCS from "../5etools-mirror-2.github.io/data/spells/spells-hwcs.json"
import IDRotF from "../5etools-mirror-2.github.io/data/spells/spells-idrotf.json"
import LLK from "../5etools-mirror-2.github.io/data/spells/spells-llk.json"
import PHB from "../5etools-mirror-2.github.io/data/spells/spells-phb.json"
import SatO from "../5etools-mirror-2.github.io/data/spells/spells-sato.json"
import SCC from "../5etools-mirror-2.github.io/data/spells/spells-scc.json"
import TCE from "../5etools-mirror-2.github.io/data/spells/spells-tce.json"
import TDCSR from "../5etools-mirror-2.github.io/data/spells/spells-tdcsr.json"
import XGE from "../5etools-mirror-2.github.io/data/spells/spells-xge.json"

export default [
	...AAG.spell,
	...AI.spell,
	...AitFRAVT.spell,
	...BMT.spell,
	...DoDk.spell,
	...EGW.spell,
	...FTD.spell,
	...GGR.spell,
	...GHLoE.spell,
	...HWCS.spell,
	...IDRotF.spell,
	...LLK.spell,
	...PHB.spell,
	...SatO.spell,
	...SCC.spell,
	...TCE.spell,
	...TDCSR.spell,
	...XGE.spell,
]
